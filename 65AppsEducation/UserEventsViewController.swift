import UIKit

class UserEventsViewController: UIViewController {

    private var colorChangeView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.blue
        view.frame.size = .init(width: 200, height: 200)
        view.center = CGPoint(x: 150, y: 150)
        return view
    }()

    private var rotatableView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.yellow
        view.frame.size = .init(width: 200, height: 150)
        view.center = CGPoint(x: 250, y: 350)
        return view
    }()

    private var movableView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.frame.size = .init(width: 200, height: 100)
        view.center = CGPoint(x: 250, y: 650)
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        let doubleTapRecognizer: UITapGestureRecognizer = .init(target: self, action: #selector(doubleTap))
        view.addSubview(colorChangeView)
        doubleTapRecognizer.numberOfTapsRequired = 2
        colorChangeView.addGestureRecognizer(doubleTapRecognizer)

        let rotationRecognizer: UIRotationGestureRecognizer = .init(target: self, action: #selector(rotate))
        view.addSubview(rotatableView)
        rotatableView.addGestureRecognizer(rotationRecognizer)

        view.addSubview(movableView)
        let longPressRecognizer: UILongPressGestureRecognizer = .init(target: self, action: #selector(draging))
        movableView.addGestureRecognizer(longPressRecognizer)

    }

    @objc private func doubleTap(_ recognizer: UITapGestureRecognizer) {
        guard let doubleTapView = recognizer.view else { return }

        if doubleTapView.backgroundColor == UIColor.blue {
            doubleTapView.backgroundColor = UIColor.red
        } else {
            doubleTapView.backgroundColor = UIColor.blue
        }
    }

    @objc private func rotate(_ recognizer: UIRotationGestureRecognizer) {
        guard let rView = recognizer.view else { return }

        rView.transform = CGAffineTransform(rotationAngle: recognizer.rotation)
    }

    @objc private func draging(_ recognizer: UILongPressGestureRecognizer) {
        guard let movableView = recognizer.view else { return }

        switch recognizer.state {
        case .began:
            movableView.frame.size.height += 5
            movableView.frame.size.width += 5
            movableView.backgroundColor = UIColor.darkGray
        case .changed:
            let location = recognizer.location(in: movableView.superview)
            var center = movableView.center
            center.x = location.x
            center.y = location.y
            movableView.center = center
        case .ended:
            movableView.frame.size.height -= 5
            movableView.frame.size.width -= 5
            movableView.backgroundColor = UIColor.black
        default:
            break
        }
    }
}

import Foundation
import CodableModels
import Networking

final class MarvelCharactersModel: ModelProtocol {

    // MARK: - Properties

    internal var userId: String?
    private var databaseService: DatabaseServiceProtocol!
    private let networkService: MarvelCharacterNetworkService?
    private var characters = [MarvelCharacter]()
    private lazy var favoritesCharacters = [String: MarvelCharacter]()
    private var constants = CodableModels.MarvelURLConstants()

    init () {
        networkService = Networking.MarvelCharacterNetworkService()
        self.getCharacters()
        guard let service: DatabaseServiceProtocol = ServiceLocator.shared.resolve() else {
            return
        }
        databaseService = service
    }

    // MARK: - Private methods

    private func getCharacters() {
        if characters.count > 0 { return }
        let url = URL(string: constants.fullURL)!
        networkService?.fetchFromURL(url: url) { data in
            do {
                let json = try JSONDecoder().decode(CodableModels.MarvelResponseData.self, from: data)
                self.characters = json.data?.results ?? [MarvelCharacter]()
            } catch {
                print(error)
            }
        } falilure: {error in
            print(error ?? "error")
        }
    }

    private func getFavorites() {
        self.databaseService.getFavoritesCharactersForUserId(userId: self.userId!) { [self] result in
            let json = result
            guard
                let data = try? JSONSerialization.data(withJSONObject: json, options: []),
                let favorites = try? JSONDecoder().decode([String: MarvelCharacter].self, from: data)
            else { return }
            favoritesCharacters = favorites
        }
    }

    // MARK: - ModelProtocol

    func getCharacterdescriptionForIndex(index: Int) -> String {
        let character = characters[index]
        return character.description ?? "No description"
    }

    func getCharactersCount() -> Int {
        return characters.count
    }

    func getNameForIndex(index: Int) -> String {
        return characters[index].name ?? "Character"
    }

    func getPictureUrlForIndex(index: Int) -> URL {
        return URL(string: characters[index].thumbnail.path! + "." + characters[index].thumbnail.ext!)!
    }

    func isFavorite(index: Int) -> Bool {
        var stringIndex = String(index)
        if stringIndex.count < 2 {
            stringIndex = "0" + stringIndex
        }
        let isFavorite = Array(favoritesCharacters.keys).contains(stringIndex)
        return isFavorite
    }

    func createAccount(userId: String, email: String, name: String) {
        favoritesCharacters = [String: MarvelCharacter]()
        self.userId = userId
    }

    func signIn(userId: String) {
        favoritesCharacters = [String: MarvelCharacter]()
        self.userId = userId
        self.getFavorites()
    }

    func getFavoriteCharacterdescriptionForIndex(index: Int) -> String {
        let character = Array(favoritesCharacters.values)[index]
        return character.description ?? "No description"
    }

    func getFavoritesCharactersCount() -> Int {
        return favoritesCharacters.count
    }

    func getFavoriteCharacterNameForIndex(index: Int) -> String {
        let character = Array(favoritesCharacters.values)[index]
        return character.name ?? "Name"
    }

    func getFavoriteCharacterPictureUrlForIndex(index: Int) -> URL {
        let character = Array(favoritesCharacters.values)[index]
        return URL(string: character.thumbnail.path! + "." + character.thumbnail.ext!)!
    }

    func addToFavorites(index: Int) {
        var stringIndex = String(index)
        if stringIndex.count < 2 {
            stringIndex = "0" + stringIndex
        }
        let character = characters[index]
        favoritesCharacters[stringIndex] = character
        guard
            let jsonData = try? JSONEncoder().encode(character),
            let jsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String: Any]
        else { return }
        databaseService.setFavoritesArray(userId: userId!, key: stringIndex, characters: jsonObject)
    }

    func getCharacterIndexForFavoritesIndex(favoritesIndex: Int) -> Int {
        let key = Array(favoritesCharacters.keys)[favoritesIndex]
        return Int(key)!
    }

    func removeFromFavorites(index: Int) {
        var stringIndex = String(index)
        if stringIndex.count < 2 {
            stringIndex = "0" + stringIndex
        }
        favoritesCharacters.removeValue(forKey: stringIndex)
        databaseService.removeValue(userId: userId!, key: stringIndex)
    }
}

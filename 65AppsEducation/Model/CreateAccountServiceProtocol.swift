import Foundation

protocol CreateAccountServiceProtocol {
    func createAccount(name: String, email: String, password: String, successComplete: @escaping (String) -> Void, falilure: @escaping (Error?) -> Void)
}

import Foundation
import Firebase
import CodableModels

final class FirebaseAuthService: CreateAccountServiceProtocol, SignInServiceProtocol, LogOutServiceProtocol {

    func logOut() throws {
        try Auth.auth().signOut()
    }

    func singIn(email: String, password: String, successComplete: @escaping (String) -> Void, falilure: @escaping (Error?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if error == nil {
                if let result = result {
                    successComplete(result.user.uid)
                }
            } else {
                falilure(error)
            }
        }
    }

    func createAccount(name: String, email: String, password: String, successComplete: @escaping (String) -> Void, falilure: @escaping (Error?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if error == nil {
                if let result = result {
                    successComplete(result.user.uid)
                }
            } else {
                falilure(error)
            }
        }
    }
}

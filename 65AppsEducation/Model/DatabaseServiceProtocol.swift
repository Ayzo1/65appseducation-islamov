import Foundation

protocol DatabaseServiceProtocol {

    func getFavoritesCharactersForUserId(userId: String, successComplete: @escaping ([String: Any]) -> Void)

    func setFavoritesArray(userId: String, key: String, characters: [String: Any])

    func removeValue(userId: String, key: String)
}

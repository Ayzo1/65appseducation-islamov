import Foundation

protocol SignInServiceProtocol: AnyObject {

    func singIn(email: String, password: String, successComplete: @escaping (String) -> Void, falilure: @escaping (Error?) -> Void)
}

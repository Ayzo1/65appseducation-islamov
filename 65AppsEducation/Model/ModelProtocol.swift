import Foundation

protocol ModelProtocol {

    var userId: String? { get set }

    func getCharacterdescriptionForIndex(index: Int) -> String

    func getCharactersCount() -> Int

    func getNameForIndex(index: Int) -> String

    func getPictureUrlForIndex(index: Int) -> URL

    func isFavorite(index: Int) -> Bool

    func createAccount(userId: String, email: String, name: String)

    func signIn(userId: String)

    func getFavoriteCharacterdescriptionForIndex(index: Int) -> String

    func getFavoritesCharactersCount() -> Int

    func getFavoriteCharacterNameForIndex(index: Int) -> String

    func getFavoriteCharacterPictureUrlForIndex(index: Int) -> URL

    func addToFavorites(index: Int)

    func removeFromFavorites(index: Int)

    func getCharacterIndexForFavoritesIndex(favoritesIndex: Int) -> Int
}

import Foundation

protocol LogOutServiceProtocol {

func logOut() throws

}

import Foundation
import Firebase

class RealtimeDatabseService: DatabaseServiceProtocol {

    var userId: String?
    let reference = Database.database(url: "https://appseducation-islamov-default-rtdb.europe-west1.firebasedatabase.app/").reference().child("users")

    func getFavoritesCharactersForUserId(userId: String, successComplete: @escaping ([String: Any]) -> Void) {
        reference.child(userId).observeSingleEvent(of: .value, with: { values in
            guard let result = values.value as? [String: Any] else {
                return
            }
            guard let favorites = result["favorites"] else {
                return
            }
            guard let characters = favorites as? [String: [String: Any]] else {
                return
            }
            successComplete(characters)
        })
    }

    func setFavoritesArray(userId: String, key: String, characters: [String: Any]) {
        reference.child(userId).child("favorites").updateChildValues([key: characters])
    }

    func removeValue(userId: String, key: String) {
        reference.child(userId).child("favorites").child(key).removeValue()
    }
}

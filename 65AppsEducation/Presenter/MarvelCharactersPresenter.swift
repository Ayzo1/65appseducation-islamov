import Foundation
import UIKit

class MarvelCharactersPresenter: MarvelCharactersPresenterProtocol {

    // MARK: - Properties

    unowned var charactersListView: CharactersListViewControllerProtocol
    var logOutService: LogOutServiceProtocol!
    var model: ModelProtocol!

    init (view: CharactersListViewControllerProtocol) {
        charactersListView = view
        guard let service: LogOutServiceProtocol = ServiceLocator.shared.resolve() else {
            return
        }
        logOutService = service
        guard let marvelModel: ModelProtocol = ServiceLocator.shared.resolve() else {
            return
        }
        model = marvelModel
    }

    // MARK: - Methods

    // MARK: - MarvelCharactersPresenterProtocol

    func logOut() throws {
        try logOutService.logOut()
    }

    func getCharacterdescriptionForIndex(index: Int) -> String {
        return model.getCharacterdescriptionForIndex(index: index)
    }

    func getCharactersCount() -> Int {
        return model.getCharactersCount()
    }

    func getNameForIndex(index: Int) -> String {
        return model.getNameForIndex(index: index)
    }

    func getPictureUrlForIndex(index: Int) -> URL {
        return model.getPictureUrlForIndex(index: index)
    }
}

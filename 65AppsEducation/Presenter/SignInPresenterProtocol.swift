import Foundation

protocol SignInPresenterProtocol {

	var signInView: SignInViewControllerProtocol { get set }

	func signIn (login: String, password: String)
}

import Foundation

protocol MarverCharacterDetailsPresenterProtocol {

    var view: CharacterDetailsViewControllerProtocol! { get set }

    func getNameForIndex(index: Int) -> String

    func getPictureUrlForIndex(index: Int) -> URL

    func getCharacterdescriptionForIndex(index: Int) -> String

    func isFavorite(index: Int) -> Bool

    func addToFavorites(index: Int)

    func removeFromFavorites(index: Int)
}

import Foundation

protocol CreateAccountPresenterProtocol {

	var createAccountView: CreateAccountViewControllerProtocol { get set }

	func createAccount(name: String, login: String, password: String, confirmPassword: String)

	func checkPasswordsIdentity(passwordA: String, passwordB: String) -> Bool
}

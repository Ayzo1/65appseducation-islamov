import Foundation
import UIKit

protocol MarvelCharactersPresenterProtocol {

    var charactersListView: CharactersListViewControllerProtocol { get set }

    func getCharactersCount() -> Int

    func getNameForIndex(index: Int) -> String

    func getPictureUrlForIndex(index: Int) -> URL

    func getCharacterdescriptionForIndex(index: Int) -> String

    func logOut() throws
}

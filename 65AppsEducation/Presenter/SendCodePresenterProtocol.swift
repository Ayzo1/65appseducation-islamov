import Foundation

protocol SendCodePresenterProtocol {

	var sendCodeView: SendCodeViewControllerProtocol { get set }

	init(sendCodeView: SendCodeViewControllerProtocol)

	func sendCode()
}

import Foundation

class SignInPresenter: SignInPresenterProtocol {

	unowned var signInView: SignInViewControllerProtocol
    var signInService: SignInServiceProtocol!
    var model: ModelProtocol!

	init(signInView: SignInViewControllerProtocol) {
		self.signInView = signInView
        guard let service: SignInServiceProtocol = ServiceLocator.shared.resolve() else {
            return
        }
        signInService = service
        guard let model: ModelProtocol = ServiceLocator.shared.resolve() else {
            return
        }
        self.model = model
	}

	func signIn(login: String, password: String) {
        if login == "" || password == "" {
            signInView.showAlert(message: "Field must not be empty")
            return
        }
        if password.count < 6 {
            signInView.showAlert(message: "Password must be at leats 6 characters")
        }
        signInService.singIn(email: login, password: password) { userId in
            self.model.signIn(userId: userId)
            self.signInView.pushToCharactersListViewController()
        } falilure: { error in
            print(error?.localizedDescription)
            self.signInView.showAlert(message: "Failed to sign in")
        }
	}
}

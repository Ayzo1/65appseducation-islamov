import Foundation

class FavoritesPresenter: FavoritesPresenterProtocol {

    unowned var view: FavoritesViewControllerProtocol
    var model: ModelProtocol!

    init(view: FavoritesViewControllerProtocol) {
        self.view = view
        guard let model: ModelProtocol = ServiceLocator.shared.resolve() else {
            return
        }
        self.model = model
    }

    func getCharactersCount() -> Int {
        return model.getFavoritesCharactersCount()
    }

    func getNameForIndex(index: Int) -> String {
        return model.getFavoriteCharacterNameForIndex(index: index)
    }

    func getPictureUrlForIndex(index: Int) -> URL {
        return model.getFavoriteCharacterPictureUrlForIndex(index: index)
    }

    func getCharacterdescriptionForIndex(index: Int) -> String {
        return model.getFavoriteCharacterdescriptionForIndex(index: index)
    }

    func getCharacterIndexForFavoritesIndex(favoritesIndex: Int) -> Int {
        return model.getCharacterIndexForFavoritesIndex(favoritesIndex: favoritesIndex)
    }
}

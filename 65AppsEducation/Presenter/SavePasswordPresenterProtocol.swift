import Foundation

protocol SavePasswordPresenterProtocol {

	var savePasswordView: SavePasswordViewControllerProtocol { get set }

	init(savePasswordView: SavePasswordViewControllerProtocol)

	func savePassword(password: String, confirmPassword: String)
}

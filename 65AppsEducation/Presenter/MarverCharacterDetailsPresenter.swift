import Foundation

class MarverCharacterDetailsPresenter: MarverCharacterDetailsPresenterProtocol {

    unowned var view: CharacterDetailsViewControllerProtocol!
    var model: ModelProtocol!

    init(view: CharacterDetailsViewControllerProtocol) {
        self.view = view
        guard let model: ModelProtocol = ServiceLocator.shared.resolve() else {
            return
        }
        self.model = model
    }

    // MARK: - MarverCharacterDetailsPresenterProtocol

    func getNameForIndex(index: Int) -> String {
        return model.getNameForIndex(index: index)
    }

    func getPictureUrlForIndex(index: Int) -> URL {
        return model.getPictureUrlForIndex(index: index)
    }

    func getCharacterdescriptionForIndex(index: Int) -> String {
        return model.getCharacterdescriptionForIndex(index: index)
    }

    func isFavorite(index: Int) -> Bool {
        return model.isFavorite(index: index)
    }

    func addToFavorites(index: Int) {
        model.addToFavorites(index: index)
    }

    func removeFromFavorites(index: Int) {
        model.removeFromFavorites(index: index)
    }
}

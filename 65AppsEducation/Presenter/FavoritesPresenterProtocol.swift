import Foundation

protocol FavoritesPresenterProtocol {

    var view: FavoritesViewControllerProtocol { get set }

    func getCharactersCount() -> Int

    func getNameForIndex(index: Int) -> String

    func getPictureUrlForIndex(index: Int) -> URL

    func getCharacterdescriptionForIndex(index: Int) -> String

    func getCharacterIndexForFavoritesIndex(favoritesIndex: Int) -> Int
}

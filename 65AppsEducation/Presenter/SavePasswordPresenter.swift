import Foundation

class SavePasswordPresenter: SavePasswordPresenterProtocol {

	unowned var savePasswordView: SavePasswordViewControllerProtocol

	required init(savePasswordView: SavePasswordViewControllerProtocol) {
		self.savePasswordView = savePasswordView
	}

	func savePassword(password: String, confirmPassword: String) {

	}
}

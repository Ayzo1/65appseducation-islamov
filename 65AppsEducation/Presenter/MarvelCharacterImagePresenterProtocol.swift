import Foundation

protocol MarvelCharacterImagePresenterProtocol {

    var view: CharacterImageViewControllerProtocol { get set }

    func getPictureUrlForIndex(index: Int) -> URL
}

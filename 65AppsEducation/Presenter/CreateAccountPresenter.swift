import Foundation

class CreateAccountPresenter: CreateAccountPresenterProtocol {

	unowned var createAccountView: CreateAccountViewControllerProtocol
    var authService: CreateAccountServiceProtocol!
    var model: ModelProtocol!

	init(createAccountView: CreateAccountViewControllerProtocol) {
		self.createAccountView = createAccountView
        guard let service: CreateAccountServiceProtocol = ServiceLocator.shared.resolve() else {
            return
        }
        self.authService = service
        guard let model: ModelProtocol = ServiceLocator.shared.resolve() else {
            return
        }
        self.model = model
	}

	func createAccount(name: String, login: String, password: String, confirmPassword: String) {
        if name == "" || login == "" || password == "" || confirmPassword == "" {
            createAccountView.showAlert(message: "Field must not be empty")
            return
        }
        if checkPasswordsIdentity(passwordA: password, passwordB: confirmPassword) {
            createAccountView.showAlert(message: "Password missmatch")
            return
        }
        authService.createAccount(name: name, email: login, password: password) { userId in
            self.model.createAccount(userId: userId, email: login, name: name)
            self.createAccountView.pushToCharactersListViewController()
        } falilure: { error in
            print(error?.localizedDescription)
            self.createAccountView.showAlert(message: "Failed to register")
        }
	}

	func checkPasswordsIdentity(passwordA: String, passwordB: String) -> Bool {
		return false
	}
}

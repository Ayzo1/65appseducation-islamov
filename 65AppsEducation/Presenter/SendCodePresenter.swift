import Foundation

class SendCodePresenter: SendCodePresenterProtocol {

	unowned var sendCodeView: SendCodeViewControllerProtocol

	required init(sendCodeView: SendCodeViewControllerProtocol) {
		self.sendCodeView = sendCodeView
	}

	func sendCode() {

	}
}

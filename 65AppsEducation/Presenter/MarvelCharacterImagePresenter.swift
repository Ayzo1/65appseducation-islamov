import Foundation

class MarvelCharacterImagePresenter: MarvelCharacterImagePresenterProtocol {

    unowned var view: CharacterImageViewControllerProtocol
    var model: ModelProtocol!

    init(view: CharacterImageViewControllerProtocol) {
        self.view = view
        guard let model: ModelProtocol = ServiceLocator.shared.resolve() else {
            return
        }
        self.model = model
    }

    func getPictureUrlForIndex(index: Int) -> URL {
        return model.getPictureUrlForIndex(index: index)
    }

}

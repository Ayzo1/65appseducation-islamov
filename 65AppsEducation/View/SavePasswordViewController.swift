import UIKit

class SavePasswordViewController: UIViewController, SavePasswordViewControllerProtocol {

	// MARK: - Properties

	var observer: KeyboardObserver?
	var presenter: SavePasswordPresenterProtocol?

	// MARK: - IBOutlets

	@IBOutlet weak var passwordTextField: TextFieldWithInset!
	@IBOutlet weak var confirmPasswordTextField: TextFieldWithInset!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var contentView: UIView!

	// MARK: - IBActions

	@IBAction func savePasswordButtonAction(_ sender: Any) {
		presenter?.savePassword(
			password: passwordTextField.text ?? "",
			confirmPassword: confirmPasswordTextField.text ?? "")
	}
	@IBAction func tap(_ sender: Any) {
        contentView.endEditing(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        observer = KeyboardObserver(scV: scrollView)
		presenter = SavePasswordPresenter(savePasswordView: self)
        navigationItem.title = "Password recovery"
    }
}

import UIKit
import Kingfisher

class CharactersListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CharactersListViewControllerProtocol {

    // MARK: - Properties

    var presenter: MarvelCharactersPresenterProtocol?
    var images = [Int: UIImage]()
    private lazy var table: UITableView = {
        let table = UITableView()
        view.addSubview(table)
        table.frame.size = table.superview?.frame.size ?? .init(width: 200, height: 200)
        table.center = table.superview?.center ?? CGPoint(x: 150, y: 150)
        table.register(UITableViewCell.self, forCellReuseIdentifier: "CharacterCell")
        return table
    }()

    @IBAction func logOutButtonAction(_ sender: Any) {
        self.present(logOutAlert, animated: true)
    }

    private lazy var logOutAlert: UIAlertController = {
        let alert = UIAlertController(title: "Log out", message: "Are you sure?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {_ in
            do {
                try self.presenter?.logOut()
            } catch {
                return
            }
            let storyboard = UIStoryboard(name: "Main", bundle: .main)
            let signInViewController = storyboard.instantiateViewController(identifier: "SignInViewController") as! SignInViewController
            let rootViewController = UINavigationController(rootViewController: signInViewController)
            let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
            sceneDelegate?.chageRootViewController(viewController: rootViewController)
        }))
        return alert
    }()

    override func viewDidLoad() {
        let button = UIBarButtonItem()
        button.tintColor = .red
        navigationItem.backButtonTitle = ""
        navigationItem.backBarButtonItem = button
        super.viewDidLoad()
        table.dataSource = self
        table.delegate = self
        presenter = MarvelCharactersPresenter(view: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        table.reloadData()

    }

    func configurateViewController() {
        self.tabBarItem.title = "Characters"
        tabBarItem.image = UIImage(systemName: "list.bullet")
        navigationItem.title = "Characters"
    }

    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getCharactersCount() ?? 0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        let characterDetailsViewController = storyboard.instantiateViewController(identifier: "CharacterDetailsViewController") as! CharacterDetailsViewController
        characterDetailsViewController.index = indexPath.row
        navigationController?.pushViewController(characterDetailsViewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "CharacterCell", for: indexPath)
        cell.textLabel?.text = presenter?.getNameForIndex(index: indexPath.row)
        let pictureUrl = presenter?.getPictureUrlForIndex(index: indexPath.row)
        let placeHolderImage = UIImage(named: "M")
        cell.imageView?.image = placeHolderImage
        let imageView = UIImageView(frame: CGRect(origin: (cell.imageView?.frame.origin)!, size: (cell.imageView?.frame.size)!))
        let processor = DownsamplingImageProcessor(size: CGSize(width: 25, height: 25))
        cell.imageView?.clipsToBounds = true
        imageView.kf.setImage(
            with: pictureUrl,
            options: [
                .processor(processor),
                .cacheOriginalImage
            ]) { result in
                switch result {
                case .success(let value):
                    self.images[indexPath.row] = value.image
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                }
            }
        cell.addSubview(imageView)
        return cell
    }

    // MARK: - CharactersListViewControllerProtocol

    func reloadTable() {
        DispatchQueue.main.async {
            self.table.reloadData()
        }
    }
}

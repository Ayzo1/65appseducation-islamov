import Foundation

protocol CharactersListViewControllerProtocol: AnyObject {

    func reloadTable()

}

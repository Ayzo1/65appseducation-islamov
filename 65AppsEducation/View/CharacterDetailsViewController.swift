import UIKit

class CharacterDetailsViewController: UIViewController, CharacterDetailsViewControllerProtocol {

    @IBOutlet var tapGesture: UITapGestureRecognizer!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    var presenter: MarverCharacterDetailsPresenterProtocol!
    var index: Int!
    var desc: String?
    var image: UIImage?
    var isFavorite: Bool!

    lazy var favoritesButton: UIBarButtonItem = {
        let button = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(favoritesButtonAction))
        button.image = UIImage(systemName: "heart")
        button.tintColor = .red
        return button
    }()

    @objc func favoritesButtonAction() {
        if isFavorite {
            self.navigationItem.rightBarButtonItem?.image = UIImage(systemName: "heart")
            presenter.removeFromFavorites(index: index)
        } else {
            self.navigationItem.rightBarButtonItem?.image = UIImage(systemName: "heart.fill")
            presenter.addToFavorites(index: index)
        }
    }

    @IBAction func tapGestureAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        let characterImageViewController = storyboard.instantiateViewController(identifier: "CharacterImageViewController") as! CharacterImageViewController
        characterImageViewController.index = index
        navigationController?.pushViewController(characterImageViewController, animated: true)
    }

    override func viewDidLoad() {
        tabBarController?.tabBar.isHidden = true
        presenter = MarverCharacterDetailsPresenter(view: self)
        let name = presenter.getNameForIndex(index: index)
        isFavorite = presenter.isFavorite(index: index)
        navigationItem.title = name
        navigationItem.rightBarButtonItem = favoritesButton
        if isFavorite {
            navigationItem.rightBarButtonItem?.image = UIImage(systemName: "heart.fill")
        }
        let button = UIBarButtonItem()
        button.tintColor = .red
        navigationItem.backButtonTitle = ""
        navigationItem.backBarButtonItem = button
        super.viewDidLoad()
        let url = presenter?.getPictureUrlForIndex(index: index)
        imageView.kf.setImage(with: url)
        desc = presenter?.getCharacterdescriptionForIndex(index: index)
        textView.text = desc
    }
}

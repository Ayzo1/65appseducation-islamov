import UIKit

class CreateAccountViewController: UIViewController, CreateAccountViewControllerProtocol {

	// MARK: - Properties

	var observer: KeyboardObserver?
	var presenter: CreateAccountPresenterProtocol?

	// MARK: - IBOutlets

	@IBOutlet weak var nameTextField: TextFieldWithInset!
	@IBOutlet weak var emailTextField: TextFieldWithInset!
	@IBOutlet weak var passwordTextField: TextFieldWithInset!
	@IBOutlet weak var confirmPasswordTextField: TextFieldWithInset!
	@IBOutlet weak var contentView: UIView!
	@IBOutlet weak var scrollView: UIScrollView!

	// MARK: - IBActions

	@IBAction func createAccountButtonAction(_ sender: Any) {
		presenter?.createAccount(
			name: nameTextField.text ?? "",
			login: emailTextField.text ?? "",
			password: passwordTextField.text ?? "",
			confirmPassword: confirmPasswordTextField.text ?? "")
	}

	@IBAction func tap(_ sender: Any) {
        contentView.endEditing(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = CreateAccountPresenter(createAccountView: self)
        observer = KeyboardObserver(scV: scrollView)
        navigationItem.title = "Create account"
    }

    func pushToCharactersListViewController() {
        let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
        guard let rootViewController = sceneDelegate?.charactersRootViewController else {
            return
        }
        sceneDelegate?.chageRootViewController(viewController: rootViewController)
    }

    func showAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

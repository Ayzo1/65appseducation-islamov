import UIKit
import Kingfisher

class CharacterImageViewController: UIViewController, UIScrollViewDelegate, CharacterImageViewControllerProtocol {

    // MARK: - Properties

    var presenter: MarvelCharacterImagePresenterProtocol?
    var index: Int!
    var imageView: UIImageView!

    override func viewDidLoad() {
        presenter = MarvelCharacterImagePresenter(view: self)
        let url = presenter?.getPictureUrlForIndex(index: index)
        super.viewDidLoad()
        imageView = UIImageView()
        imageView.frame = CGRect(origin: view.frame.origin, size: view.frame.size)
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleAspectFill
        view.addSubview(imageView)
        imageView.kf.setImage(with: url)
        let pinchRecognizer: UIPinchGestureRecognizer = .init(target: self, action: #selector(zoomAction))
        let panRecognizer: UIPanGestureRecognizer = .init(target: self, action: #selector(draging))
        imageView.addGestureRecognizer(pinchRecognizer)
        imageView.addGestureRecognizer(panRecognizer)
    }

    // MARK: - Selectors

    @objc private func draging(_ recognizer: UIPanGestureRecognizer) {
        guard let view = recognizer.view else {
            return
        }
        switch recognizer.state {
        case .began, .changed:
            let delta = recognizer.translation(in: view)
            var center = view.center
            center.x += delta.x
            center.y += delta.y
            recognizer.setTranslation(.zero, in: view.superview)
            view.center = center
        default:
            break
        }
    }

    @objc private func zoomAction(_ sender: UIPinchGestureRecognizer) {
        let scaleResult = sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale)
        guard let scale = scaleResult else {
            return
        }
        sender.view?.transform = scale
        sender.scale = 1
      }
}

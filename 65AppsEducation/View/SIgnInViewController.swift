import UIKit

class SignInViewController: UIViewController, SignInViewControllerProtocol {

	// MARK: - Properties

	var observer: KeyboardObserver?
	var presenter: SignInPresenterProtocol?

	// MARK: - IBOutlets

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var contentView: UIView!
	@IBOutlet weak var emailTextField: TextFieldWithInset!
	@IBOutlet weak var passwordTextField: TextFieldWithInset!

	// MARK: - IBActions

	@IBAction func signInButtonAction(_ sender: Any) {
		presenter?.signIn(
			login: emailTextField.text ?? "",
			password: passwordTextField.text ?? "")
	}

	@IBAction func tap(_ sender: Any) {
        contentView.endEditing(true)
    }

    override func viewDidLoad() {
        presenter = SignInPresenter(signInView: self)
        super.viewDidLoad()
        observer = KeyboardObserver(scV: scrollView)
    }

    func pushToCharactersListViewController() {
        let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
        guard let rootViewController = sceneDelegate?.charactersRootViewController else {
            return
        }
        sceneDelegate?.chageRootViewController(viewController: rootViewController)
    }

    func showAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

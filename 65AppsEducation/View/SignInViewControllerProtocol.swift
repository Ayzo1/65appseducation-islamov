import Foundation

protocol SignInViewControllerProtocol: AnyObject {

    func pushToCharactersListViewController()

    func showAlert(message: String)
}

import Foundation

protocol CreateAccountViewControllerProtocol: AnyObject {
    func pushToCharactersListViewController()

    func showAlert(message: String)
}

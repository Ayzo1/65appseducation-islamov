import UIKit

class SendCodeViewController: UIViewController, SendCodeViewControllerProtocol {

	// MARK: - Properties

	var observer: KeyboardObserver?
	var presenter: SendCodePresenterProtocol?

	// MARK: - IBOutlets

	@IBOutlet weak var emailTextField: TextFieldWithInset!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var contentView: UIView!

	// MARK: - IBActions

	@IBAction func sendCodeButtonAction(_ sender: Any) {
		presenter?.sendCode()
	}

	@IBAction func tap(_ sender: Any) {
        contentView.endEditing(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        observer = KeyboardObserver(scV: scrollView)
		presenter = SendCodePresenter(sendCodeView: self)
        navigationItem.title = "Password recovery"
    }
}

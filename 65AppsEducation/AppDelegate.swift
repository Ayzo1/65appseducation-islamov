//
//  AppDelegate.swift
//  65AppsEducation
//
//  Created by ayaz on 10.10.2021.
//

import UIKit
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        let databaseService: DatabaseServiceProtocol = RealtimeDatabseService()
        ServiceLocator.shared.register(service: databaseService)
        let model: ModelProtocol = MarvelCharactersModel()
        let authService = FirebaseAuthService()
        let signIn: SignInServiceProtocol = authService
        let createAccount: CreateAccountServiceProtocol = authService
        let logOut: LogOutServiceProtocol = authService
        ServiceLocator.shared.register(service: model)
        ServiceLocator.shared.register(service: signIn)
        ServiceLocator.shared.register(service: createAccount)
        ServiceLocator.shared.register(service: logOut)
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}

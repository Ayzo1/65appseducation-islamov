Pod::Spec.new do |spec|


  spec.name         = "CodableModels"
  spec.version      = "0.0.1"
  spec.summary      = "CodableModels"


  spec.description  = <<-DESC
		CodableModels
                   DESC

  spec.homepage     = "https://gitlab.com/Ayzo1/65appseducation-islamov"
  spec.ios.deployment_target = "12.1"
  spec.swift_version = "4.2"
  spec.license      = "BSD"
  spec.author       = { "Ayzo1" => "ayaz.islamov@icloud.com" }
 

  spec.source       =  { :path => "." }

  spec.source_files  = "CodableModels/**/*.{h,m,swift,xib,storyboard}"

end

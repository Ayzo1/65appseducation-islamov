import Foundation

public struct Thumbnail: Codable {
    public var path: String?
    public var ext: String?

    enum CodingKeys: String, CodingKey {
        case ext = "extension"
        case path
    }
}

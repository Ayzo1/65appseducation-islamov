import Foundation

public struct MarvelURLConstants {
    
    public init() {
        
    }
    
    let publicKey: String = "67339ff0e953523101e40496c807529d"
    let hash: String = "8fac52c193c8554bbf37ec5bc9448284"
    let timeStamp: String = "1"
    let marvelURL: String = "https://gateway.marvel.com"
    let charactersListURL: String = "/v1/public/characters"
    lazy var auth: String = "?ts=\(timeStamp)&apikey=\(publicKey)&hash=\(hash)"
    public lazy var fullURL: String = marvelURL + charactersListURL + auth
}

import Foundation
import UIKit

public struct MarvelCharacter: Codable {

    public var id: Int?
    public var name: String?
    public var description: String?
    public var thumbnail: Thumbnail
}

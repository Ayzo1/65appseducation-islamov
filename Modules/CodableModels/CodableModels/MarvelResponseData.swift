import Foundation

public struct MarvelResponseData: Codable {
    public let data: MarvelResponseResults?
}

import Foundation

public struct MarvelResponseResults: Codable {
    public let results: [MarvelCharacter]?
}
